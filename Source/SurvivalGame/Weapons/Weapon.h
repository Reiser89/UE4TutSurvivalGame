// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

class UAnimMontage;
class ASurvivalCharacter;
class UAudioComponent;
class UParticleSystemComponent;
class UCameraShake;
class UForceFeedbackEffect;
class USoundCue;

UENUM(BlueprintType)
enum class EWeaponState : uint8
{
	Idle,
	Firing,
	Reloading,
	Equipping
};

USTRUCT(BlueprintType)
struct FWeaponData
{
	GENERATED_USTRUCT_BODY()

	// Clip size
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ammo)
	int32 AmmoPerClip;

	// The item that this weapon uses as ammo
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ammo)
	TSubclassOf<class UAmmoItem> AmmoClass;

	// Time between two consecutive shots
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = WeaponStat)
	float TimeBetweenShots;

	// defaults
	FWeaponData()
	{
		AmmoPerClip = 20;
		TimeBetweenShots = 0.2f;
	}
};

USTRUCT()
struct FWeaponAnim
{
	GENERATED_USTRUCT_BODY()

	// Animation played on pawn (1st person view)
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	UAnimMontage* Pawn1P;
	 
	// Animation played on pawn (3rd person view)
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	UAnimMontage* Pawn3P;
};

USTRUCT(BlueprintType)
struct FHitScanConfiguration
{
	GENERATED_BODY()
	FHitScanConfiguration()
	{
		Distance = 10000.f;
		Damage = 25.f;
		Radius = 0.f;
		DamageType = UDamageType::StaticClass();
	}

	// A map of bone -> damage amount. If the bone is a child of the given bone, it will use this damage amount.
	// A value of 2 would mean double damage etc.
	UPROPERTY(EditDefaultsOnly, Category = "Tryce Info")
	TMap<FName, float> BoneDamageModifiers;

	// How far the hitscan traced for a hit
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Trace Info")
	float Distance;

	// How far the hitscan traced for a hit
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Trace Info")
	float Damage;

	// How far the hitscan traced for a hit
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Trace Info")
	float Radius;

	// Type of damage
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	TSubclassOf<UDamageType> DamageType;
};

UCLASS()
class SURVIVALGAME_API AWeapon : public AActor
{
	GENERATED_BODY()

	friend class ASurvivalCharacter; // Character can access any function of this class, so we dont have to do extra Get Set functions
	
public:	
	// Sets default values for this actor's properties
	AWeapon();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void PostInitializeComponents() override;
	virtual void BeginPlay() override;
	virtual void Destroyed() override;

protected:

	// Consume a bullet
	void UseClipAmmo();

	// Consume ammo from inventory
	void ConsumeAmmo(const int32 Amount);

	// [server] return ammo to the inventory when the weapon is unequipped
	void ReturnAmmoToInventory();

	// Weapon is being equipped by owner pawn
	virtual void OnEquip();

	// Weapon is now equipped by owner pawn
	virtual void OnEquipFinished();

	// Weapon is holstered by owner pawn
	virtual void OnUnEquip();

	// Check if it's currently equipped
	bool IsEquipped() const;

	// Check if mesh is already attached
	bool IsAttachedToPawn() const;

	//////////////////////////////////////////////////////////////////////////
	// Input

	// [local + server] start weapon fire
	virtual void StartFire();

	// [local + server] stop weapon fire
	virtual void StopFire();

	// [all] start weapon reload
	virtual void StartReload(bool bFromReplication = false);

	// [local + server] interrupt weapon reload
	virtual void StopReload();

	// [server] performs actual reload
	virtual void ReloadWeapon();

	// Trigger reload from server
	UFUNCTION(reliable, client)
	void ClientStartReload();

	bool CanFire() const;
	bool CanReload() const;

	UFUNCTION(BlueprintPure, Category = "Weapon")
	EWeaponState GetCurrentState() const;

	// Get current ammo amount (total)
	UFUNCTION(BlueprintPure, Category = "Weapon")
	int32 GetCurrentAmmo() const;

	// Get current ammo amount (clip)
	UFUNCTION(BlueprintPure, Category = "Weapon")
	int32 GetCurrentAmmoInClip() const;

	// Get clip size
	int32 GetAmmoPerClip() const;

	// Get weapon mesh (needs pawn owner to determine variant)
	UFUNCTION(BlueprintPure, Category = "Weapon")
	USkeletalMeshComponent* GetWeaponMesh() const;

	// Get pawn owner
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	class ASurvivalCharacter* GetPawnOwner() const;

	// Set the weapon's owning pawn
	void SetPawnOwner(ASurvivalCharacter* SurvivalCharacter);

	// Gets last time when this weapon was switched to
	float GetEquipStartedTime() const;

	// Gets the duration of equipped weapon
	float GetEquipDuration() const;

protected:

	// The weapon item in the players inventory
	UPROPERTY(Replicated, BlueprintReadOnly, Transient)
	class UWeaponItem* Item;

	// Pawn owner
	UPROPERTY(Transient, ReplicatedUsing = OnRep_PawnOwner)
	class ASurvivalCharacter* PawnOwner;

	// Weapon data
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FWeaponData WeaponConfig;

	// Line trace data. Will be used if projectile class is null
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FHitScanConfiguration HitScanConfig;

public:

	// Weapon mesh - The one which gets attached to the player
	UPROPERTY(EditAnywhere, Category = Components)
	USkeletalMeshComponent* WeaponMesh;

protected:

	// Adjustment to handle frame rate affecting actual timer interval.
	UPROPERTY(Transient)
	float TimerIntervalAdjustment;

	// Whether to allow automatic weapons to catch up with shorter refire cycles
	UPROPERTY(Config)
	bool bAllowAutomaticWeaponCatchup = true;

	// Firing audio (bLoopedFireSound set)
	UPROPERTY(Transient)
	UAudioComponent* FireAC;

	// Name of bone/socket for muzzle in weapon mesh
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FName MuzzleAttachPoint;

	// Name of the socket to attach to the character on
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FName AttachSocket1P;

	// Name of the socket to attach to the character on
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FName AttachSocket3P;

	// FX for muzzle flash
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UParticleSystem* MuzzleFX;

	// spawned component for muzzle FX
	UPROPERTY(Transient)
	UParticleSystemComponent* MuzzlePSC;

	// spawned component for second muzzle FX (Needed for split screen)
	UPROPERTY(Transient)
	UParticleSystemComponent* MuzzlePSCSecondary;

	// camera shake on firing
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	TSubclassOf<UCameraShake> FireCameraShake;

	//The time it takes to aim down sights, in seconds
	UPROPERTY(EditDefaultsOnly, Category = Weapon)
	float ADSTime;

	// The amount of recoil to apply. We choose a random point from 0-1 on the curve and use it to drive recoil.
	// This means designers get lots of control over the recoil pattern
	UPROPERTY(EditDefaultsOnly, Category = Recoil)
	class UCurveVector* RecoilCurve;

	// The speed at which the recoil bumps up per second
	UPROPERTY(EditDefaultsOnly, Category = Recoil)
	float RecoilSpeed;

	// The speed at which the recoil resets per second
	UPROPERTY(EditDefaultsOnly, Category = Recoil)
	float RecoilResetSpeed;

	// Force feedback effect to play when the weapon is fired - Controler vibration
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UForceFeedbackEffect* FireForceFeedback;

	// Single fire sound (bLoopedFireSound not set)
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* FireSound;

	// Looped fire sound (bLoopedFireSound set)
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* FireLoopSound;

	// Finished burst sound (bLoopedFireSound set)
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* FireFinishSound;

	// Out of ammo sound
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* OutOfAmmoSound;

	// Reload sound
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* ReloadSound;

	// Reload animations
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	FWeaponAnim ReloadAnim;

	// Equip sound
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* EquipSound;

	// Equip animations
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	FWeaponAnim EquipAnim;

	// Fire animations
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	FWeaponAnim FireAnim;

	// Fire animations
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	FWeaponAnim FireAimingAnim;

	// Is muzzle FX looped?
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	uint32 bLoopedMuzzleFX : 1;

	// Is fire sound looped?
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	uint32 bLoopedFireSound : 1;

	// Is fire animation looped?
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	uint32 bLoopedFireAnim : 1;

	// Is fire animation playing?
	uint32 bPlayingFireAnim : 1;

	// Is weapon currently equipped?
	uint32 bIsEquipped : 1;

	// Is weapon fire active?
	uint32 bWantsToFire : 1;

	// Is reload animation playing?
	UPROPERTY(Transient, ReplicatedUsing = OnRep_Reload)
		uint32 bPendingReload : 1;

	// Is equip animation playing?
	uint32 bPendingEquip : 1;

	// Weapon is refiring
	uint32 bRefiring;

	// Current weapon state
	EWeaponState CurrentState;

	// Time of last successful weapon fire
	float LastFireTime;

	// Last time when this weapon was switched to
	float EquipStartedTime;

	// How much time weapon needs to be equipped
	float EquipDuration;

	// Current ammo - inside clip
	UPROPERTY(Transient, Replicated)
	int32 CurrentAmmoInClip;

	// Burst counter, used for replicating fire events to remote clients
	UPROPERTY(Transient, ReplicatedUsing = OnRep_BurstCounter)
	int32 BurstCounter;

	// Handle for efficient management of OnEquipFinished timer
	FTimerHandle TimerHandle_OnEquipFinished;

	// Handle for efficient management of StopReload timer
	FTimerHandle TimerHandle_StopReload;

	// Handle for efficient management of ReloadWeapon timer
	FTimerHandle TimerHandle_ReloadWeapon;

	// Handle for efficient management of HandleFiring timer
	FTimerHandle TimerHandle_HandleFiring;

	//////////////////////////////////////////////////////////////////////////
	// Input - server side

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerStartFire();

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerStopFire();

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerStartReload();

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerStopReload();

	//////////////////////////////////////////////////////////////////////////
	// Replication & effects
	UFUNCTION()
	void OnRep_PawnOwner(); // When someone takes the gun

	UFUNCTION()
	void OnRep_BurstCounter(); // Replicating the firing FX

	UFUNCTION()
	void OnRep_Reload();

	// Called in network play to do the cosmetic fx for firing
	virtual void SimulateWeaponFire();

	// Called in network play to stop cosmetic fx (e.g. for a looping shot).
	virtual void StopSimulatingWeaponFire();

	//////////////////////////////////////////////////////////////////////////
	// Weapon usage


	// Handle hit locally before asking server to process hit
	void HandleHit(const FHitResult& Hit, class ASurvivalCharacter* HitPlayer = nullptr);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerHandleHit(const FHitResult& Hit, class ASurvivalCharacter* HitPlayer = nullptr);

	// [local] weapon specific fire implementation
	virtual void FireShot();

	// [server] fire & update ammo
	UFUNCTION(reliable, server, WithValidation)
	void ServerHandleFiring();

	// [local + server] handle weapon refire, compensating for slack time if the timer can't sample fast enough
	void HandleReFiring();

	// [local + server] handle weapon fire
	void HandleFiring();

	// [local + server] firing started
	virtual void OnBurstStarted();

	// [local + server] firing finished
	virtual void OnBurstFinished();

	// Update weapon state
	void SetWeaponState(EWeaponState NewState);

	// Determine current weapon state
	void DetermineWeaponState();

	// Attaches weapon mesh to pawn's mesh
	void AttachMeshToPawn();


	//////////////////////////////////////////////////////////////////////////
	// Weapon usage helpers

	// Play weapon sounds
	UAudioComponent* PlayWeaponSound(USoundCue* Sound);

	// Play weapon animations
	float PlayWeaponAnimation(const FWeaponAnim& Animation);

	// Stop playing weapon animations
	void StopWeaponAnimation(const FWeaponAnim& Animation);

	// Get the aim of the camera
	FVector GetCameraAim() const;

	// Find hit
	FHitResult WeaponTrace(const FVector& StartTrace, const FVector& EndTrace) const;

};
